# -*- coding: utf-8 -*-
{
    'name': "Profile Webform SWM LLA",

    'summary': """
        Customisations for SWM
        """,

    'description': """
    """,

    'author': "Hucke Media GmbH & Co. KG",
    'website': "http://www.hucke-media.com",
    'category': 'Custom',
    'version': '0.1',
    'depends': ['base', 'website', 'web_studio'],
    'data': [
        'views/assets_frontend.xml',
        'views/theme.xml',
    ],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
