# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo.http import request
from odoo import fields, http, _
import datetime
from odoo.exceptions import ValidationError
import ast
import logging
_logger = logging.getLogger(__name__)


class SWMLLAWebsiteResPartner(http.Controller):

    @http.route(['/kontaktformular'], type='http', auth="public", website=True,
                sitemap=False)
    def swm_lla_page(self,  **post):
        return request.render('profile_webform_swm_lla.swm_lla_body')
